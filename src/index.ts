let x: number = 10;
let isAdmin: boolean = false;
let namee: string = 'Alisson';
const myMumbers: number[] = [1,2,3,4,5,3,4]; 

const myObj: {name: string,idade: number, email: string} = {
    name: 'teste',
    idade: 22,
    email: 'rwarwaarw'
}
x = 15;

console.log(typeof namee);
console.log(typeof myMumbers);
console.log(myObj);

//Union type

let id: string | number = '10';

id = 200

// type alias
type myType = string | number | boolean;

const productID: myType = 'terss';

//funcao

function soma(a: number, b: number): number{
    return a + b
}

function sayHelloTo(name: string): string{
    return "hello, " + name;
}

console.log(soma(12,12));
console.log(sayHelloTo('Alissão'));

//interface

interface MathFunctionParams{
    n1: number,
    n2: number
}

function sum (nums: MathFunctionParams) : number{
    return nums.n1 + nums.n2;
}

//interface